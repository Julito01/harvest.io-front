import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { environment } from '../../../environments/environment';
import { SensorData } from '../interfaces/sensor.interface';


@Injectable({
  providedIn: 'root'
})
export class SensorsService {
  constructor(private http: HttpClient) { }

  getSensorData(): Promise<SensorData[]> {
    return firstValueFrom(this.http.get<SensorData[]>(`${environment.apiUrl}/sensor/`)) as Promise<SensorData[]>;
  }
}
