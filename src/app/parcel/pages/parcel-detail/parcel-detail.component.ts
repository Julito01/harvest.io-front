import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { ParcelService } from '../../services/parcel.service'
import { ParcelData } from '../../interfaces/parcel.interface'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'

@Component({
  selector: 'app-parcel-detail',
  templateUrl: './parcel-detail.component.html',
  styleUrls: ['./parcel-detail.component.css']
})
export class ParcelDetailComponent implements OnInit {
  public parcel: ParcelData = <ParcelData>{
    parcelId: '',
    soilType: '',
    size: 0,
    ph: 0
  }
  public isEditView: boolean = false;
  public parcelForm: FormGroup = this.formBuilder.group({
    parcelId: [this.parcel.parcelId],
    soilType: [this.parcel.soilType, Validators.required],
    size: [this.parcel.size],
    ph: [this.parcel.ph]
  })

  constructor(private route: ActivatedRoute, private parcelService: ParcelService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.parcelForm.disable()
    const { id } = this.route.snapshot.params
    this.parcelService.getParcelById(id).then((parcel: ParcelData) => {
      this.parcel = parcel
      this.updateForm(this.parcel)
    })
  }

  public updateForm(parcel: ParcelData) {
    this.parcelForm.patchValue(parcel)
  }

  public activateEdit() {
    this.parcelForm.enable()
    this.isEditView = true
  }
}
