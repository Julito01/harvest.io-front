import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { SensorListRoutingModule } from './sensor-routing.module'
import { SensorDetailComponent } from './pages/sensor-detail/sensor-detail.component'
import { SensorListComponent } from './pages/sensor-list/sensor-list.component'

@NgModule({
  declarations: [SensorDetailComponent, SensorListComponent],
  imports: [
    CommonModule,
    SensorListRoutingModule
  ],
})
export class SensorListModule { }
