import { Component, OnInit } from '@angular/core'
import { ParcelService } from 'src/app/parcel/services/parcel.service'
import { ParcelData } from '../../interfaces/parcel.interface'
@Component({
  selector: 'app-parcel',
  templateUrl: './parcel-list.component.html',
  styleUrls: ['./parcel-list.component.css']
})
export class ParcelComponent implements OnInit {
  public parcelData: ParcelData[] = [];

  constructor(private parcelService: ParcelService) { }

  ngOnInit(): void {
    this.parcelService.getParcelData()
    .then((data: ParcelData[]) => {
      this.parcelData = data
    })
   }
}
