import { Component, OnInit } from '@angular/core'
import { SensorsService } from 'src/app/sensor/services/sensor.service'
import { SensorData } from '../../interfaces/sensor.interface'


@Component({
  selector: 'app-sensor',
  templateUrl: './sensor-list.component.html',
  styleUrls: ['./sensor-list.component.css']
})
export class SensorListComponent implements OnInit {
  public sensorData: SensorData[] = [];

  constructor(private sensorService: SensorsService) { }

  ngOnInit(): void {
    this.sensorService.getSensorData()
      .then((data: SensorData[]) => {
        this.sensorData = data
      })
  }
}
