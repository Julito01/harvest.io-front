import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { firstValueFrom } from 'rxjs'
import { environment } from '../../../environments/environment'
import { ParcelData } from '../interfaces/parcel.interface'

@Injectable({
  providedIn: 'root'
})
export class ParcelService {
  constructor(private http: HttpClient) { }

  getParcelData(): Promise<ParcelData[]> {
    return firstValueFrom(this.http.get<ParcelData[]>(`${environment.apiUrl}/parcel/`)) as Promise<ParcelData[]>
  }

  getParcelById(id: String): Promise<ParcelData> {
    return firstValueFrom(this.http.get(`${environment.apiUrl}/parcel/${id}`)) as Promise<ParcelData>
  }
}
