import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { SensorListComponent } from './pages/sensor-list/sensor-list.component'
import { SensorDetailComponent } from './pages/sensor-detail/sensor-detail.component'

const routes: Routes = [
  {
    path: '',
    component: SensorListComponent
  },
  {
    path: ':id',
    component: SensorDetailComponent
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SensorListRoutingModule { }
