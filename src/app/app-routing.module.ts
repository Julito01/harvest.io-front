import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'


const routes: Routes = [
  {
    path: 'home',
    loadChildren:() => import('./home/home.module').then(module => module.HomeModule)
  },
  {
    path: 'sensor', loadChildren: () => import('./sensor/sensor.module').then(module => module.SensorListModule)
  },
  {
    path: 'parcel',
    loadChildren: () => import('./parcel/parcel.module').then(module => module.ParcelModule)
  },
  {
    path: 'login',
    loadChildren: () => import ('./login/login.module').then (module => module.LoginModule)
  },
  {
    path: '**',
    redirectTo: 'home',
    pathMatch: 'full'
  }
]


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
