import { RouterModule, Routes } from "@angular/router";
import {LoginComponent} from './login.component'
import { NgModule } from "@angular/core";
import { ContactComponent } from "./pages/contact.component";

const routes: Routes = [

    {
        path: '',
        component: LoginComponent
    },
    {
        path: 'contact',
        component: ContactComponent
    }

]

@NgModule ({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class LoginRoutingModule {}
