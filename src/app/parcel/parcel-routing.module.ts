import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ParcelComponent } from './pages/parcel-list/parcel-list.component';
import { ParcelDetailComponent } from './pages/parcel-detail/parcel-detail.component';
import { ParcelNewComponent } from './pages/parcel-new/parcel-new.component';

const routes: Routes = [
  {
    path: '',
    component: ParcelComponent
  },
  {
    path: 'new',
    component: ParcelNewComponent
  },
  {
    path: ':id',
    component: ParcelDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParcelRoutingModule { }
