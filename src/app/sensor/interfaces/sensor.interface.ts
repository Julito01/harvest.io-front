export class SensorData {
  sensorId: string
  soilMoisture: number

  constructor(sensorId: string, soilMoisture: number) {
    this.sensorId = sensorId
    this.soilMoisture = soilMoisture
  }
}
