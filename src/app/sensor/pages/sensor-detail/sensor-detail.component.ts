import { Component, OnInit } from '@angular/core'
import { Router, ActivatedRoute, ParamMap } from '@angular/router'

@Component({
  selector: 'app-sensor-detail',
  templateUrl: './sensor-detail.component.html',
  styleUrls: ['./sensor-detail.component.css']
})
export class SensorDetailComponent implements OnInit {
  public sensorId: String = ''

  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    const { id } = this.route.snapshot.params
    this.sensorId = id
  }

}
