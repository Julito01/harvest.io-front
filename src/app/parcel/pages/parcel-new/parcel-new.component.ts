import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-parcel-new',
  templateUrl: './parcel-new.component.html',
  styleUrls: ['./parcel-new.component.css']
})
export class ParcelNewComponent implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    console.log("Se cargo parcel new");
  }

}
