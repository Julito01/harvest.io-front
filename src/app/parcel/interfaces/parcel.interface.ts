export class ParcelData {
  parcelId: string
  soilType: string
  size: number
  ph: number

  constructor(parcelId: string, soilType: string, size: number, ph: number) {
    this.parcelId = parcelId
    this.soilType = soilType
    this.size = size
    this.ph = ph
  }
}
