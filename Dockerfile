FROM node:18.15.0-alpine AS node
WORKDIR /app
COPY package*.json ./
RUN npm install

CMD npm start
