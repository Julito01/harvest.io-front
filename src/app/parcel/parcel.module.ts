import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ParcelRoutingModule } from './parcel-routing.module'
import { ParcelComponent } from './pages/parcel-list/parcel-list.component'
import { ParcelDetailComponent } from './pages/parcel-detail/parcel-detail.component'
import { ParcelNewComponent } from './pages/parcel-new/parcel-new.component'
import { ReactiveFormsModule } from '@angular/forms'

@NgModule({
  declarations: [ParcelDetailComponent, ParcelComponent, ParcelNewComponent],
  imports: [
    CommonModule,
    ParcelRoutingModule,
    ReactiveFormsModule
  ]
})
export class ParcelModule { }
