# Harvest.io

```
Aplicación para parametrizar datos sobre humedad del suelo, temperatura ambiente, PH del suelo, etc. de Huertas e Invernaderos.
Apuntado para pequeños productores.
```

## Levantar proyecto localmente

Para levantar el proyecto debe tener instalado **_Docker_** en su computadora.
`https://www.docker.com/products/docker-desktop/`

## Clonar repositorio

Crear una carpeta para alojar el código fuente de la aplicación.
Luego, ingresar a esa carpeta usando `cd {nombre_carpeta}`
Dentro de la carpeta utilizar el siguiente comando

```
git clone https://gitlab.com/Julito01/harvest.io-front.git
```

## Acceda al directorio clonado en una terminal

```
cd harvest.io-front
```

## Ingresar comandos _Docker_

```
docker-compose up
```

## Ingresar a la aplicación

Abra una navegador web e ingrese a `localhost:4200`
